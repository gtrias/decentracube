import * as CUBE_COLORS from '../constants/cubeColors'

export default class Square {
  getColor() {
    return CUBE_COLORS.RED
  }
}
