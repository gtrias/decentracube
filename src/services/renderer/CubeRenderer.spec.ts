import CubeRenderer from './CubeRenderer'

import { expect } from 'chai'
import 'mocha'

describe('services/renderer', () => {
  describe('CubeRenderer', () => {

    it('should return number of sides', () => {
      const cubeRenderer = new CubeRenderer()
      expect(cubeRenderer.render()).to.not.be.undefined
    })

  })
})
