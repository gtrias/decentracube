import Cube from '../../model/Cube'

export default interface RenderInterface {
  cube: Cube
  render(): any
}
