import Side from './Side'
import { expect } from 'chai'
import 'mocha'

describe('models', () => {
  describe('Side', () => {

    it('should return number of sides', () => {
      const side = new Side()
      // Each 3x3 side should have 9 squares
      expect(side.getSquares().length).to.equal(9)
    })

  })
})
