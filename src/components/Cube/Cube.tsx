import Cube from '../../model/Cube'
import CubeRenderer from '../../services/renderer/CubeRenderer'

export function BuildCube(props: any) {
  const cube = new Cube()
  const cubeRenderer = new CubeRenderer(cube)

  return cubeRenderer.render()
}
