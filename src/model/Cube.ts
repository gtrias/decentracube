import Side from './Side'

export default class Cube {
  /**
   * We should to create an array of sides of the cube in the next form
   *   1
   * 2 3 4
   *   5
   *   6
   */
  getSides(): Array<Side> {
    return [
      new Side(),
      new Side(),
      new Side(),
      new Side(),
      new Side(),
      new Side(),
    ]
  }
}
