import Square from './Square'

export default class Side {
  getSquares(): Array<Square> {
    return [
      new Square(),
      new Square(),
      new Square(),
      new Square(),
      new Square(),
      new Square(),
      new Square(),
      new Square(),
      new Square(),
    ]
  }
}
