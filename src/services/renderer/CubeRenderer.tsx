import * as DCL from 'metaverse-api'

import Cube from '../../model/Cube'
import RenderInterface from './RendererInterface'

export default class CubeRenderer implements RenderInterface {
  cube: Cube

  constructor(cube: Cube) {
    this.cube = cube
  }

  render() {
    const cubeX = 3
    const cubeY = 3
    const cubeZ = 3

    const totalBoxes = []

    let colRotation = 0

    for (let x = 0; x < cubeX; x++) {
      for (let y = 0; y < cubeY; y++) {
        for (let z = 0; z < cubeZ; z++) {
          const color = colRotation % 2 === 0 ? '#FFFFFF' : '#000000'
          totalBoxes.push(
            <box
              scale={{x: 1, y: 1, z: 1}}
              position={{ x: x, y: y, z: z }}
              color={ color }
            />
          )

          colRotation++
        }
      }
    }

    return (
      <entity position={{ x: -1, y: 1, z: 0 }}>
        { totalBoxes }
      </entity>
    )
  }
}
