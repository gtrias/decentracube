import { expect } from 'chai'
import 'mocha'
import * as CUBE_COLORS from '../constants/cubeColors'

import Square from './Square'

describe('models', () => {
  describe('Square', () => {

    it('should return number of sides', () => {
      const square = new Square()
      expect(square.getColor()).to.equal(CUBE_COLORS.RED)
    })

  })
})
