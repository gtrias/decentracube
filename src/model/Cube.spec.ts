import Cube from './Cube'
import { expect } from 'chai'
import 'mocha'

describe('models', () => {
  describe('Cube', () => {

    it('should return number of sides', () => {
      const cube = new Cube()
      expect(cube.getSides().length).to.equal(6)
    })

  })
})
